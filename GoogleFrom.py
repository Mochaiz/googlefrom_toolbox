import pandas as pd
import pandasql as pandasql

df = pd.read_csv(r'Book6.csv')

df[df == 'หวาน']  = 'ประจำ'
df[df == 'มัน']  = 'ค่อนข้างบ่อย'
df[df == 'เค็ม']  = 'นานๆครั้ง'
df[df == 'จืด']  = 'ไม่เคย'

df[df == 'ประจำ']  = 3
df[df == 'ค่อนข้างบ่อย']  = 2
df[df == 'นานๆครั้ง']  = 1
df[df == 'ไม่เคย']  = 0
df[df == 'ค่อยข้างบ่อย'] = 2

df.rename(columns={'1.ตามปกติท่านชอบรับประทานอาหารแบบใดมากที่สุด':'ตามปกติท่านชอบรับประทานอาหารแบบใดมากที่สุด'},inplace= True)
df.rename(columns={'2.รับประทานอาหารมากกว่า 3 มื้อต่อวันหรือไม่':'รับประทานอาหารมากกว่า 3 มื้อต่อวันหรือไม่'},inplace= True)
df.rename(columns={'3.รับประทานอาหารจานด่วน เช่น พิซซ่า ไก่ทอด':'รับประทานอาหารจานด่วน เช่น พิซซ่า ไก่ทอด'},inplace= True)
df.rename(columns={'4.รับประทานเนื้อสัตว์ติดมัน เช่น ขาหมู คอหมูย่าง หนังไก่ทอด':'รับประทานเนื้อสัตว์ติดมัน เช่น ขาหมู คอหมูย่าง หนังไก่ทอด'},inplace= True)
df.rename(columns={'5.รับประทานอาหารสำเร็จรูป เช่น มาม่า ยำยำ ปลากระป๋อง':'รับประทานอาหารสำเร็จรูป เช่น มาม่า ยำยำ ปลากระป๋อง'},inplace= True)
df.rename(columns={'6.รับประทานแกงกะทิหรือขนมหวานใส่กระทิ เช่น แกงเขียวหวานแกงบวช':'รับประทานแกงกะทิหรือขนมหวานใส่กระทิ เช่น แกงเขียวหวานแกงบวช'},inplace= True)
df.rename(columns={'7.รับประทาน อาหารจำพวกทอด เช่น ประท่องโก๋ กล้วยทอด หมูทอด':'รับประทาน อาหารจำพวกทอด เช่น ประท่องโก๋ กล้วยทอด หมูทอด'},inplace= True)
df.rename(columns={'8.รับประทานผลไม้ที่มีรสหวานจัด เช่น ทุเรียน ละมุด มะม่วงสุข แอปเปิ้ล':'รับประทานผลไม้ที่มีรสหวานจัด เช่น ทุเรียน ละมุด มะม่วงสุข แอปเปิ้ล'},inplace= True)
df.rename(columns={'9.กินขนมที่มีรสหวาน เช่น ทองหยิบ ทองหยอด ฝอยทอง ขนมถ้วยฟู':'กินขนมที่มีรสหวาน เช่น ทองหยิบ ทองหยอด ฝอยทอง ขนมถ้วยฟู'},inplace= True)
df.rename(columns={'10.รับประทานอาหารขบเคี้ยว เช่น ขนมมันฝรั่งทอด ':'รับประทานอาหารขบเคี้ยว เช่น ขนมมันฝรั่งทอด'},inplace= True)
df.rename(columns={'11.ดื่มน้ำอัดลม':'ดื่มน้ำอัดลม'},inplace= True)
df.rename(columns={'12.รับประทานอาหารประเภทของหมักดอง เช่น ปลาร้า ไม้แช่อิ่ม ผลไม้ดอง':'รับประทานอาหารประเภทของหมักดอง เช่น ปลาร้า ไม้แช่อิ่ม ผลไม้ดอง'},inplace= True)
df.rename(columns={'13.รับประทานอาหารตอนดึก':'รับประทานอาหารตอนดึก'},inplace= True)
df.rename(columns={'14.ดื่มเครื่องดื่มที่แอลกอฮอล์ เช่น เบียร์ เหล้า ไวน์':'ดื่มเครื่องดื่มที่แอลกอฮอล์ เช่น เบียร์ เหล้า ไวน์'},inplace= True)
df.rename(columns={'15.ดื่มชา/กาแฟ':'ดื่มชา/กาแฟ'},inplace= True)
df.rename(columns={'16.นมและเครื่องดื่มต่างๆ ที่เขียนข้างตัวผลิตภัณฑ์ว่าไขมัน 0% หรือ Low fat':'นมและเครื่องดื่มต่างๆ ที่เขียนข้างตัวผลิตภัณฑ์ว่าไขมัน 0% หรือ Low fat'},inplace= True)
df.rename(columns={'17.รับประทานขนมเบเกอรี่ ขนมอบต่างๆ เช่น เค้ก ':'รับประทานขนมเบเกอรี่ ขนมอบต่างๆ เช่น เค้ก'},inplace= True)
df.rename(columns={'18.ทานขนม และลูกกวาด':'ทานขนม และลูกกวาด'},inplace= True)
df.rename(columns={'19.รับประทานอาหารจุบจิบ(ของว่างระหว่างวัน)':'รับประทานอาหารจุบจิบ(ของว่างระหว่างวัน)'},inplace= True)
df.rename(columns={'20.การอดมื้อเช้า':'การอดมื้อเช้า'},inplace= True)


'''def TransposeMatrix(A):
    return [[A[j][i] for j in range(len(A))] for i in range(len(A[0]))]


b = df.columns
a = [[i for i in df[b[j]]] for j in range(23)]
c = TransposeMatrix(a)

d = [sum(c[i][4:]) for i in range(len(c))]
print(d)'''


a = df.loc[1:,['เพศ','อายุ']]

b = df.loc[1:,['ตามปกติท่านชอบรับประทานอาหารแบบใดมากที่สุด','รับประทานอาหารมากกว่า 3 มื้อต่อวันหรือไม่','รับประทานอาหารจานด่วน เช่น พิซซ่า ไก่ทอด',
               'รับประทานเนื้อสัตว์ติดมัน เช่น ขาหมู คอหมูย่าง หนังไก่ทอด','รับประทานอาหารสำเร็จรูป เช่น มาม่า ยำยำ ปลากระป๋อง','รับประทานแกงกะทิหรือขนมหวานใส่กระทิ เช่น แกงเขียวหวานแกงบวช',
               'รับประทาน อาหารจำพวกทอด เช่น ประท่องโก๋ กล้วยทอด หมูทอด','รับประทานผลไม้ที่มีรสหวานจัด เช่น ทุเรียน ละมุด มะม่วงสุข แอปเปิ้ล',
               'กินขนมที่มีรสหวาน เช่น ทองหยิบ ทองหยอด ฝอยทอง ขนมถ้วยฟู','รับประทานอาหารขบเคี้ยว เช่น ขนมมันฝรั่งทอด', 'ดื่มน้ำอัดลม',
               'รับประทานอาหารประเภทของหมักดอง เช่น ปลาร้า ไม้แช่อิ่ม ผลไม้ดอง','รับประทานอาหารตอนดึก','ดื่มเครื่องดื่มที่แอลกอฮอล์ เช่น เบียร์ เหล้า ไวน์', 'ดื่มชา/กาแฟ',
               'นมและเครื่องดื่มต่างๆ ที่เขียนข้างตัวผลิตภัณฑ์ว่าไขมัน 0% หรือ Low fat','รับประทานขนมเบเกอรี่ ขนมอบต่างๆ เช่น เค้ก', 'ทานขนม และลูกกวาด',
               'รับประทานอาหารจุบจิบ(ของว่างระหว่างวัน)','การอดมื้อเช้า']]

a['Sum'] = b.sum(axis=1)


a['FatRate'] =  ''
for i in range(1,101):
    a.FatRate[i] = (a.Sum[i]*100)/60

#print(a)

#Excel for visulize

c = df.loc[1:,['เพศ','ตามปกติท่านชอบรับประทานอาหารแบบใดมากที่สุด','รับประทานอาหารมากกว่า 3 มื้อต่อวันหรือไม่','รับประทานอาหารจานด่วน เช่น พิซซ่า ไก่ทอด',
               'รับประทานเนื้อสัตว์ติดมัน เช่น ขาหมู คอหมูย่าง หนังไก่ทอด','รับประทานอาหารสำเร็จรูป เช่น มาม่า ยำยำ ปลากระป๋อง','รับประทานแกงกะทิหรือขนมหวานใส่กระทิ เช่น แกงเขียวหวานแกงบวช',
               'รับประทาน อาหารจำพวกทอด เช่น ประท่องโก๋ กล้วยทอด หมูทอด','รับประทานผลไม้ที่มีรสหวานจัด เช่น ทุเรียน ละมุด มะม่วงสุข แอปเปิ้ล',
               'กินขนมที่มีรสหวาน เช่น ทองหยิบ ทองหยอด ฝอยทอง ขนมถ้วยฟู','รับประทานอาหารขบเคี้ยว เช่น ขนมมันฝรั่งทอด', 'ดื่มน้ำอัดลม',
               'รับประทานอาหารประเภทของหมักดอง เช่น ปลาร้า ไม้แช่อิ่ม ผลไม้ดอง','รับประทานอาหารตอนดึก','ดื่มเครื่องดื่มที่แอลกอฮอล์ เช่น เบียร์ เหล้า ไวน์', 'ดื่มชา/กาแฟ',
               'นมและเครื่องดื่มต่างๆ ที่เขียนข้างตัวผลิตภัณฑ์ว่าไขมัน 0% หรือ Low fat','รับประทานขนมเบเกอรี่ ขนมอบต่างๆ เช่น เค้ก', 'ทานขนม และลูกกวาด',
               'รับประทานอาหารจุบจิบ(ของว่างระหว่างวัน)','การอดมื้อเช้า']]

print(c)
c.to_csv('Score.csv', encoding ='utf8',index = False) #ตารางแสดงคะแนนที่เก็บมา
a.to_csv('FatSc.csv', encoding='utf8', index=False)







